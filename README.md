#About

ioregwv is Apple Inc.'s ioreg tool that's been revised to print it's output to a JSON data format suitable for the IORegistry Web Viewer.

Source code is from IOKitTools-86 at [opensource.apple.com](http://opensource.apple.com/source/IOKitTools/IOKitTools-86/).

#Build Notes

Successful compilation of this requires the inclusion of [IOKitLibPrivate.h](http://www.opensource.apple.com/source/IOKitUser/IOKitUser-647.6.10/IOKitLibPrivate.h)

#License
[APSL](http://www.publicsource.apple.com/license/apsl/)